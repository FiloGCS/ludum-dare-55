using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    //References
    NavMeshAgent agent;
    public HashSet<Slime> followers;
    public int team = -1;
    public PlayerInput playerInput;
    public List<GameObject> playerSkins;
    public GameObject playerRadiusReference;
    public Renderer playerSpriteReference;
    public LineRenderer aimLineReference;

    //Input
    public bool useJoystickDirection = false;
    public float phaseOffset = 0f;
    public float joystickPhaseOffset = -2.32f;
    public float cleanAimDirectionThreshold = 0.1f;

    //DEBUG
    public TMPro.TextMeshPro debugText;
    public Vector3 debugTextOffset = new Vector3(0f, 2.0f, 0f);

    //Grab Rate
    public float baseGrabRate = 4f;
    public float baseGrabRadius = 3f;
    public float maxGrabRadius = 5f;
    //Grab Radius
    public float grabRadiusGrowthRate = 1f;
    public float grabRadius = 0;

    //PRIVATE
    private float grabProgress = 0f;
    private Vector2 moveDirection;
    private Vector2 aimDirection;
    private Vector2 cleanAimDirection;
    public float aimLerpSnappiness = 0.05f;
    public Altar aimedAltar;

    private void Start() {
        //Init
        GM.Instance.RegisterLeader(this);
        agent = this.GetComponent<NavMeshAgent>();
        followers = new HashSet<Slime>();

        //Player Input
        //Set correct player skin
        print(playerInput.playerIndex.ToString());
        for(int i = 0; i < playerSkins.Count; i++) {
            playerSkins[i].SetActive(i == playerInput.playerIndex);
        }
        //Set correct player team
        team = playerInput.playerIndex;
        //Move me to my altar
        this.transform.position = GM.Instance.GetAltarsByTeam(this.team).ToList<Altar>()[0].transform.position;
        //Make me look somewhere
        cleanAimDirection = Vector3.right;
        //DEBUG
        if (GM.Instance.DebugMode) {
            GameObject textObject = new GameObject("UnitStateText");
            debugText = textObject.AddComponent<TextMeshPro>();

            // Set text properties
            debugText.text = "Leader";
            debugText.fontSize = 4;
            debugText.alignment = TextAlignmentOptions.Center;
            debugText.color = Color.white;

            // Position the text object above the unit
            debugText.transform.SetParent(transform);  // Make the text object a child of the unit
            debugText.transform.localPosition = debugTextOffset;  // Adjust position relative to the unit

        }
    }
    private void Update() {
        //Update sprite depending on the rotation
        Quaternion targetRotation = Quaternion.LookRotation(new Vector3(cleanAimDirection.x, 0f, cleanAimDirection.y));
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Mathf.Clamp(Time.deltaTime/ aimLerpSnappiness, 0,1));
        UpdateFlipbookSprite();
        //Update aimed altar highlight
        if (aimedAltar) {
            Vector3 d = (aimedAltar.transform.position - this.transform.position).normalized;
            aimLineReference.SetPosition(0, this.transform.position + d/2f + Vector3.up * 0.25f);
            aimLineReference.SetPosition(1, aimedAltar.transform.position - d + Vector3.up * 0.25f);
            aimLineReference.transform.rotation = Quaternion.identity;
            aimLineReference.transform.position = Vector3.zero;
            aimLineReference.gameObject.SetActive(true);
        } else {
            aimLineReference.gameObject.SetActive(false);
        }
    }
    private void FixedUpdate() {

        if(GM.Instance.gameState == GameState.Playing) {

            //DEBUG
            if (GM.Instance.DebugMode) {
                debugText.text = followers.Count.ToString();
                debugText.transform.rotation = Quaternion.LookRotation(debugText.transform.position - GM.Instance.cam.transform.position);
            }

            //Read Input
            moveDirection = this.GetComponent<PlayerInput>().actions["Move"].ReadValue<Vector2>();
            aimDirection = this.GetComponent<PlayerInput>().actions["Aim"].ReadValue<Vector2>();
            //Aiming
            //Override aim direction if the magnitude is small
            Vector2 newCleanAimDirection = (aimDirection.sqrMagnitude < cleanAimDirectionThreshold) ? moveDirection : aimDirection;
            if (newCleanAimDirection.sqrMagnitude > 0.1f) {
                cleanAimDirection = newCleanAimDirection;
            }
            Vector3 aim = new Vector3(cleanAimDirection.x, 0f, cleanAimDirection.y);
            //Pick aimed altar
            aimedAltar = GM.Instance.GetClosestAltarToLine(this.transform.position, aim);
            Debug.DrawLine(this.transform.position, aimedAltar.transform.position, Color.red);

            //rotation
            //this.transform.rotation = Quaternion.LookRotation(new Vector3(cleanAimDirection.x, 0f, cleanAimDirection.y));

            //movement
            if (moveDirection != Vector2.zero) {
                Vector3 targetPosition = this.transform.position + new Vector3(moveDirection.x, 0f, moveDirection.y);
                agent.SetDestination(targetPosition);
                if (GM.Instance.DebugMode) {
                    Debug.DrawLine(this.transform.position, targetPosition, Color.green);
                }
            }
            //grab
            float isGrabbing = this.GetComponent<PlayerInput>().actions["Grab"].ReadValue<float>();
            if (isGrabbing > 0.2f) {
                //Grow the Grab Radius
                grabRadius += grabRadiusGrowthRate * Time.fixedDeltaTime;
                if (grabRadius > maxGrabRadius) {
                    grabRadius = maxGrabRadius;
                }
                //Show Grab Radius Reference
                playerRadiusReference.SetActive(true);
                playerRadiusReference.transform.localScale = new Vector3(grabRadius, grabRadius, grabRadius);
                //Progress in the grabbing
                grabProgress += baseGrabRate * Time.fixedDeltaTime;
                if (grabProgress >= 1) {
                    grabProgress--;
                    GrabSomething();
                }
            } else {
                playerRadiusReference.SetActive(false);
                grabProgress = 0;
                grabRadius = baseGrabRadius;
            }
        }

    }

    public HashSet<Slime> GetAllMyUnits() {
        return GM.Instance.GetSlimesByTeam(team);
    }
    public void GrabUnit(Slime unit) {
        if (followers.Add(unit)) {
            unit.SetState(UnitState.following);
        }
    }
    public void GrabUnits(HashSet<Slime> units) {
        foreach (Slime u in units) {
            GrabUnit(u);
        }
    }
    public bool GrabSomething() {
        //try to grab a slime
        foreach(Slime s in GetAllMyUnits()) {
            //If there's a slime of mine in range
            if (Vector3.Distance(s.transform.position, this.transform.position) < grabRadius) {
                //If the slime is not already following
                if(s.state != UnitState.following) {
                    GrabUnit(s);
                    return true;
                }
            }
        }
        //try to grab from a random garrison in range
        foreach(Altar a in GM.Instance.GetAltarsByTeam(team)) {
            if(Vector3.Distance(a.transform.position, this.transform.position) < grabRadius) {
                Slime s = a.Ungarrison();
                if (s != null) {
                    GrabUnit(s);
                    return true;
                }
            }
        }
        //I have not found anything in range
        //TODO - Add some floating text or VFX/SFX
        return false;
    }

    //Drop
    public void DropUnit(Slime unit) {
        if (followers.Remove(unit)) {
            unit.Drop();
        }
    }
    public void DropUnits(HashSet<Slime> units) {
        List<Slime> buffer = units.ToList<Slime>();
        foreach (Slime u in buffer) {
            DropUnit(u);
        }
    }
    //Throw
    public void ThrowUnit(Slime unit) {
        if (followers.Remove(unit)) {
            unit.Throw(aimedAltar);
        }
    }
    public void ThrowUnits(HashSet<Slime> units) {
        List<Slime> buffer = units.ToList<Slime>() ;
        foreach (Slime u in buffer) {
            ThrowUnit(u);
        }
    }

    //Input Events
    public void OnGrab() {
        //
    }
    public void OnThrow() {
        ThrowUnits(followers);
    }
    public void OnDrop() {
        DropUnits(GetAllMyUnits());
    }
    public void OnRestart() {
        print("RESTART");
        SceneManager.LoadScene(0);
    }

    public void UpdateFlipbookSprite() {
        float angle = this.transform.rotation.eulerAngles.y / 360;
        if (useJoystickDirection) {
            Vector3 dir = new Vector3(cleanAimDirection.x, 0f, cleanAimDirection.x);
            angle = Quaternion.FromToRotation(new Vector3(1, 0, 0), dir).eulerAngles.y/360;
            angle -= joystickPhaseOffset;
        }
        //HACK - 
        angle -= phaseOffset;
        angle += 0.5f;
        //Send the value to the material for any active skins
        foreach (GameObject skin in playerSkins) {
            if (skin.activeSelf) {
                skin.GetComponent<Renderer>().material.SetFloat("_FlipbookIndex", angle);
            }
        }
    }

}
