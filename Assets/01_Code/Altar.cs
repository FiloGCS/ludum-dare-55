using System.Collections.Generic;
using UnityEngine;

public class Altar : MonoBehaviour {

    //Team related
    public int team = -1;
    public int garrison = 10;
    public List<GameObject> skins;
    public List<Transform> orbSprites;
    public GameObject noTeamSkin;
    public TMPro.TextMeshPro GarrisonText;
    public Transform OrbSprite;
    public float minOrbScale;
    public float maxOrbScale;
    public float maxOrbScaleValue;

    private float nextSlimeProduction = 0;
    private float productionRate = 0.5f;


    // Start is called before the first frame update
    void Start() {
        //Init
        GM.Instance.RegisterAltar(this);
        //Choose the correct skin
        UpdateSkin();

    }

    // Update is called once per frame
    void Update() {
        GarrisonText.text = garrison.ToString();
        float lambda = garrison / maxOrbScaleValue;
        lambda = Mathf.Clamp01(lambda);
        lambda = Mathf.Lerp(minOrbScale, maxOrbScale, lambda);
        Vector3 scale = new Vector3(lambda, lambda, lambda);
        OrbSprite.localScale = scale;
    }

    private void FixedUpdate() {
        if(GM.Instance.gameState == GameState.Playing) {
            if (team != -1) {
                nextSlimeProduction += (productionRate + Mathf.Clamp01(garrison * 0.05f)) * Time.fixedDeltaTime;
                if (nextSlimeProduction >= 1.0f) {
                    ProduceSlime();
                    nextSlimeProduction -= 1.0f;
                }
            }
        }
    }

    public void ProduceSlime() {
        garrison++;
        //Vector3 randomOffset = new Vector3(Random.Range(0f, 1f), 0f, Random.Range(0f, 1f));
        //Slime.SpawnSlime(this.transform.position + randomOffset, this.team);
    }

    public Slime Ungarrison() {
        if(garrison > 1) {
            garrison--;
            Vector3 randomOffset = new Vector3(Random.Range(0f, 1f), 0f, Random.Range(0f, 1f));
            return Slime.SpawnSlime(this.transform.position + randomOffset, this.team);
        } else {
            return null;
        }
    }

    public void Garrison(Slime slime) {
        if (slime.team == this.team) {
            garrison+= slime.level;
            slime.GarrisonSlime();
        } else {
            garrison-= slime.level*2;
            slime.SacrificeSlime();
            //TODO - If garrison == 0;
            if(garrison <= 0) {
                garrison = Mathf.Abs(garrison);
                SetTeam(slime.team);
            }
        }
    }

    public void SetTeam(int newTeam) {
        team = newTeam;
        UpdateSkin();
    }

    public void UpdateSkin() {
        //No Team
        if (team == -1) {
            noTeamSkin.SetActive(true);
            GarrisonText = noTeamSkin.GetComponentInChildren<TMPro.TextMeshPro>();
            OrbSprite = orbSprites[2];
        } else {
            noTeamSkin.SetActive(false);
        }
        //Any Team
        for (int i = 0; i < 2; i++) {
            //Activate the correct skin
            if (i == team) {
                skins[i].SetActive(true);
                OrbSprite = orbSprites[i];
                //Reference the correct garrison text
                GarrisonText = skins[i].GetComponentInChildren<TMPro.TextMeshPro>();
            } else {
                skins[i].SetActive(false);
            }
        }
    }
}
