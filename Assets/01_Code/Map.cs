using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Tilemaps;

public class Map : MonoBehaviour {
    public List<GameObject> tileGameObjects;
    public List<Vector3Int> neutralAltars;
    public List<Vector3Int> team_0_Altars;
    public List<Vector3Int> team_1_Altars;
    public List<Vector3Int> mustHaveTiles;

    public Grid grid;
    public List<Tile> tiles;
    public float tileSize = 1;
    public int mapSize = 10;
    public float lacunarity = 0.2f;

    
    public NavMeshData navMeshData;
    public NavMeshDataInstance navMeshDataInstance;
    public NavMeshBuildSettings navMeshBuildSettings;


    // Start is called before the first frame update
    void Start() {
        //Init self reference in GM
        GM.Instance.map = this;
        //Init grid
        grid = GetComponent<Grid>();
        grid.cellSize = new Vector3(tileSize * 0.8659766f, tileSize, tileSize);
        //Spawn all tiles
        for (int i = -mapSize / 2; i < mapSize / 2; i++) {
            for (int j = -mapSize / 2; j < mapSize / 2; j++) {
                //Pick random tile prefab
                bool mustExist = false;
                Vector3Int gridPos = new Vector3Int(i, j, 0);
                mustExist = (neutralAltars.Contains(gridPos) ||
                    team_0_Altars.Contains(gridPos) ||
                    team_1_Altars.Contains(gridPos) ||
                    mustHaveTiles.Contains(gridPos));
                if (mustExist || Random.Range(0, 1f) > lacunarity) {
                    int index = Random.Range(0, tileGameObjects.Count);
                    Vector3 pos = grid.CellToWorld(new Vector3Int(i, j, 0));
                    GameObject newTile = Instantiate(tileGameObjects[index], pos, Quaternion.identity, this.transform);
                    newTile.transform.localScale = new Vector3(tileSize, tileSize, tileSize);
                    newTile.GetComponent<Tile>().gridPosition = new Vector3Int(i, j, 0);
                    tiles.Add(newTile.GetComponent<Tile>());
                }
            }
        }
        ////Kill some random tiles
        //for(int i = 0; i < 15; i++) {
        //    Tile destroyedTile = tiles[Random.Range(0, tiles.Count)];
        //    tiles.Remove(destroyedTile);
        //    Destroy(destroyedTile.gameObject);
        //}
        //Build Navmesh
        navMeshBuildSettings = NavMesh.GetSettingsByID(0); // Default settings
        navMeshData = new NavMeshData();
        navMeshDataInstance = NavMesh.AddNavMeshData(navMeshData);
        BuildNavMesh();
        //Update GM Navmesh
        GM.Instance.SetNavmesh(GetComponent<NavMeshSurface>());

        //Spawn Altars
        foreach(Vector3Int pos in neutralAltars) {
            GameObject newAltar = Instantiate(GM.Instance.altarPrefab, grid.CellToWorld(pos), Quaternion.identity, this.transform);
            newAltar.GetComponent<Altar>().team = -1;
            newAltar.GetComponent<Altar>().garrison = Random.Range(1, 10);
        }
        foreach (Vector3Int pos in team_0_Altars) {
            GameObject newAltar = Instantiate(GM.Instance.altarPrefab, grid.CellToWorld(pos), Quaternion.identity, this.transform);
            newAltar.GetComponent<Altar>().team = 0;
        }
        foreach (Vector3Int pos in team_1_Altars) {
            GameObject newAltar = Instantiate(GM.Instance.altarPrefab, grid.CellToWorld(pos), Quaternion.identity, this.transform);
            newAltar.GetComponent<Altar>().team = 1;
        }


        //Kill some empty tiles around the center

    }

    public void BuildNavMesh() {
        // Assuming you have a list of sources, such as terrain, obstacles, etc.
        var sources = new List<NavMeshBuildSource>();
        var bounds = new Bounds(transform.position, new Vector3(500, 50, 500)); // Adjust size as necessary

        NavMeshBuilder.CollectSources(bounds, LayerMask.GetMask("Default"), NavMeshCollectGeometry.PhysicsColliders, 0, new List<NavMeshBuildMarkup>(), sources);
        NavMeshBuilder.UpdateNavMeshData(navMeshData, navMeshBuildSettings, sources, bounds);

        // If you have multiple NavMesh surfaces you need to manually handle which one agents use.
        // This is just setting the whole scene to use this one for simplicity.
    }

    public bool IsPositionInBounds(Vector3Int pos) { //UNTESTED
        if (pos.x > mapSize / 2 || pos.x <= -mapSize / 2 || pos.y > mapSize / 2 || pos.y <= -mapSize / 2) {
            return false;
        }
        return true;
    }

    public Tile GetTileAtPosition(Vector3Int pos) {
        foreach(Tile t in tiles) {
            if(t.gridPosition == pos) {
                return t;
            }
        }
        return null;
    }

    private void OnDestroy() {
        navMeshDataInstance.Remove();
    }

    public float GetPhaseFromGridDirection(Vector3Int position, Vector3Int direction) {
        if(direction == new Vector3Int(1, 0, 0)) {
            //Always X+ which means phase 0
            return 0;
        }
        if(direction == new Vector3Int(-1, 0, 0)) {
            //Always X- which means phase 180
            return 180;
        }
        if(direction == new Vector3Int(0, 1, 0)) {
            //Depends if we're odd or even
            if(position.y%2 == 0) {
                //Even means North East (phase 60)
                return 60;
            } else {
                //Odd means North West (phase 120)
                return 120;
            }
        }
        if(direction == new Vector3Int(0, -1, 0)) {
            //Depends if we're odd or even
            if (position.y % 2 == 0) {
                //Even means South East (phase 300)
                return 300;
            } else {
                //Odd means South West (phase 240)
                return 240;
            }
        }
        return 0;
    }

}
