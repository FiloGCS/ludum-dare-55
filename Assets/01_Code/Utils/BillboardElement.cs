using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardElement : MonoBehaviour
{
    public bool runInUpdate = false;
    void Start()
    {
        this.transform.rotation = GM.Instance.cam.transform.rotation;
    }
    private void Update() {
        if (runInUpdate) {
            this.transform.rotation = GM.Instance.cam.transform.rotation;
        }
    }
}
