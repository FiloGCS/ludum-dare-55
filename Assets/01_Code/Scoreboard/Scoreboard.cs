using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    public TMPro.TextMeshPro score0;
    public TMPro.TextMeshPro score1;
    public Renderer scorebar;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GM.Instance.gameState == GameState.Playing) {
            int t0Score = GM.Instance.GetTeamScore(0);
            if (score0) {
                score0.text = t0Score.ToString();
            }
            int t1Score = GM.Instance.GetTeamScore(1);
            if (score1) {
                score1.text = t1Score.ToString();
            }
            float lambda = (float)t0Score / (float)(t0Score + t1Score);
            scorebar.material.SetFloat("_Lambda", lambda);
        }
    }
}
