using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public enum GameState { Loading, Playing, Pause, Count, None};

public class GM : Singleton<GM> {

    public bool DebugMode = true;

    public HashSet<Slime> slimes;
    public HashSet<Player> leaders;
    public HashSet<Altar> altars;

    public Camera cam;
    public NavMeshSurface navmesh;
    public Map map;

    public GameObject altarPrefab;
    public GameObject slimePrefab;

    public GameState gameState = GameState.Loading;

    private void Start() {
        cam = Camera.main;
        slimes = new HashSet<Slime>();
        leaders = new HashSet<Player>();
        altars = new HashSet<Altar>();
    }

    private void FixedUpdate() {
        switch (gameState) {
            case GameState.Loading:
                if (leaders.Count >= 1) { //TODO - Limit players
                    gameState = GameState.Playing;
                }
                break;
            case GameState.Playing:

                break;
            case GameState.Pause:
                break;
            case GameState.Count:
                break;
            case GameState.None:
                break;
            default:
                break;
        }
    }


    //Unit Management
    public HashSet<Slime> GetSlimesByTeam(int team) {
        HashSet<Slime> result = new HashSet<Slime>();
        foreach (Slime u in slimes) {
            if (u.team == team) {
                result.Add(u);
            }
        }
        return result;
    }
    public HashSet<Altar> GetAltarsByTeam(int team) {
        HashSet<Altar> result = new HashSet<Altar>();
        foreach (Altar a in altars) {
            if (a.team == team) {
                result.Add(a);
            }
        }
        return result;
    }
    public int GetTeamScore(int team) {
        int score = 0;
        //Add altars
        foreach (Altar a in altars) {
            if(a.team == team) {
                score += a.garrison;
            }
        }
        //Add slimes
        foreach(Slime s in slimes) {
            if (s.team == team) {
                score += s.level;
            }
        }
        return score;
    }
    public Altar GetClosestAltar(Vector3 pos) {
        Altar closestAltar = null;
        float closestDistance = Mathf.Infinity;
        foreach (Altar a in altars) {
            float currentDistance = Vector3.Distance(a.transform.position, pos);
            if (closestAltar == null || closestDistance > currentDistance) {
                closestAltar = a;
                closestDistance = currentDistance;
            }
        }
        return closestAltar;
    }
    public Slime GetClosestAlliedSlime(Slime me) {
        Slime closestAlliedSlime = null;
        float closestDistance = Mathf.Infinity;
        foreach (Slime s in GetSlimesByTeam(me.team)) {
            if (s != me) {
                float currentDistance = Vector3.Distance(s.transform.position, me.transform.position);
                if (currentDistance < closestDistance) {
                    closestAlliedSlime = s;
                    closestDistance = currentDistance;
                }
            }
        }
        return closestAlliedSlime;
    }
    public Altar GetClosestAltarToLine(Vector3 origin, Vector3 direction) { //TODO - HACK - Last moment "fix"
        Altar closestAltar = null;
        float closestDistance = Mathf.Infinity;
        foreach (Altar a in altars) {
            float dot = Vector3.Dot(direction, (a.transform.position - origin).normalized);
            float currentDistance = DistancePointToLine(a.transform.position, origin + direction * 2f, direction) - (dot*5);
            if (closestAltar == null || closestDistance > currentDistance) {
                closestAltar = a;
                closestDistance = currentDistance;
            }
        }
        return closestAltar;

    }
    public bool RegisterUnit(Slime newUnit) {
        return slimes.Add(newUnit);
    }
    public bool RemoveUnit(Slime removedUnit) {
        return slimes.Remove(removedUnit);
    }
    public bool RegisterLeader(Player newLeader) {
        return leaders.Add(newLeader);
    }
    public bool RegisterAltar(Altar newAltar) {
        return altars.Add(newAltar);
    }
    public float DistancePointToLine(Vector3 point, Vector3 lOrigin, Vector3 lDirection) {
        lDirection = lDirection.normalized;
        Ray ray = new Ray(lOrigin, lDirection);
        return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
    }
    //Team Management
    public Player GetLeaderByTeam(int team) {
        foreach (Player l in leaders) {
            if (l.team == team) {
                return l;
            }
        }
        return null;
    }

    public void SetNavmesh(NavMeshSurface newNavmesh) {
        navmesh = newNavmesh;
        var agents = FindObjectsOfType<NavMeshAgent>();
        foreach (var agent in agents) {
            agent.enabled = false;
            agent.enabled = true; // Re-enable to force the agent to recalculate its path with the new NavMesh
            agent.Warp(agent.transform.position);
        }
    }
}
