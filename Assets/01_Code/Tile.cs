using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType {Field, Altar, Void}

public class Tile : MonoBehaviour {

    public TileType type;

    public Vector3Int gridPosition;

    public GameObject EdgePrefab;
    public List<GameObject> edges;

    private void Start() {
        this.gameObject.name = gridPosition.ToString();
        SpawnEdges();
    }

    public void SpawnEdges() {
        for(int i = 0; i < 6; i++) {
            float angle = 0 - i*60f;
            Vector3 offset = new Vector3(1f, 0f, 0f) * GM.Instance.map.grid.cellSize.y;
            offset = Quaternion.Euler(0, angle, 0) * offset;
            Vector3 targetPosition = this.transform.position + offset;
            Vector3Int neighbourPosition = GM.Instance.map.grid.WorldToCell(targetPosition);
            if (GM.Instance.map.GetTileAtPosition(neighbourPosition) != null) {
                edges[i].SetActive(false);
                //SpawnEdge(angle);
            }
        }
    }

    public void SpawnEdge(float phase) {
        Vector3 pos = this.transform.position + new Vector3(0, 0.01f, 0);
        Quaternion rot = Quaternion.Euler(0, -phase, 0);
        GameObject newEdge = Instantiate(EdgePrefab, pos, rot, this.transform);
        //newEdge.transform.localScale = new Vector3(50, 50, 50);
    }
}
