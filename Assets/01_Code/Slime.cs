using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using static UnityEngine.UI.CanvasScaler;

public enum UnitState { idle, following, charging, garrisoning }

public class Slime : MonoBehaviour {
    NavMeshAgent agent;

    //Team related
    public int team = -1;
    public List<GameObject> skins;

    public Player leader;
    public UnitState state = UnitState.idle;
    public int level = 1;
    public float mergeDistance = 0.1f;
    public float mergeChance = 0.5f;

    public float followSpeed = 8f;
    public float chargeSpeed = 3f;
    public float idleSpeed = 1f;
    public float chargingDetectionRange = 2f;
    public Altar targetAltar = null;

    private Vector3 chargeDirection = Vector3.zero;

    //DEBUG
    public TMPro.TextMeshPro debugText;
    public Vector3 debugTextOffset = new Vector3(0, 2.0f, 0);


    void Start() {
        //Init
        GM.Instance.RegisterUnit(this);
        agent = this.GetComponent<NavMeshAgent>();
        //Set the correct team skin
        UpdateSkin();


        //DEBUG
        if (GM.Instance.DebugMode) {
            GameObject textObject = new GameObject("UnitStateText");
            debugText = textObject.AddComponent<TextMeshPro>();


            // Set text properties
            debugText.text = state.ToString();
            debugText.fontSize = 4;
            debugText.alignment = TextAlignmentOptions.Center;
            debugText.color = Color.black;

            // Position the text object above the unit
            debugText.transform.SetParent(transform);  // Make the text object a child of the unit
            debugText.transform.localPosition = debugTextOffset;  // Adjust position relative to the unit
        }

        agent = GetComponent<NavMeshAgent>();
    }

    void FixedUpdate() {
        //DEBUG
        if (GM.Instance.DebugMode) {
            debugText.transform.rotation = Quaternion.LookRotation(debugText.transform.position - GM.Instance.cam.transform.position);
        }

        switch (state) {
            case UnitState.idle:
                //todo something (maybe slowly steer away?)
                //Maybe do this only every couple of frames?
                LookForTargets();
                break;
            case UnitState.following:
                //Check random chance to try to merge
                if (Random.Range(0f, 1f) < mergeChance) {
                    CheckForMerges();
                }
                if (leader == null) {
                    leader = GM.Instance.GetLeaderByTeam(team);
                } else {
                    agent.SetDestination(leader.transform.position);
                }
                break;
            case UnitState.charging:
                //If I see an altar in range, go to the closest one
                Altar foundAltar = LookForTargets();
                //If I don't see any
                if(!foundAltar){
                    //Keep moving along charging direction
                    if (chargeDirection != Vector3.zero) {
                        Vector3 targetPosition = this.transform.position + new Vector3(chargeDirection.x, 0f, chargeDirection.z) * 5f;
                        agent.SetDestination(targetPosition);
                    }
                }
                break;
            case UnitState.garrisoning:
                //If collide with an altar:
                if (Vector3.Distance(this.transform.position, targetAltar.transform.position) < 1f) {

                    targetAltar.Garrison(this);
                }
                break;
            default:
                break;
        }
    }

    public void SetState(UnitState newState) {
        if (this != null) {
            state = newState;
            switch (newState) {
                case UnitState.idle:
                    SetHighlight(false);
                    agent.speed = idleSpeed;
                    break;
                case UnitState.following:
                    SetHighlight(true);
                    if (agent) {//HACK - ????
                        agent.speed = followSpeed + Random.Range(-0.5f, 0.5f);
                    }
                    break;
                case UnitState.charging:
                    SetHighlight(false);
                    agent.speed = chargeSpeed + Random.Range(-0.5f, 0.5f);
                    break;
                case UnitState.garrisoning:
                    SetHighlight(false);
                    agent.speed = chargeSpeed + Random.Range(1f, 2f);
                    break;
                default:
                    break;
            }
        }
    }

    public void Drop() {
        SetState(UnitState.idle);
        //Scatter
        //agent.SetDestination(this.transform.position + new Vector3(Random.Range(-2f, 2f), 0f, Random.Range(-2f, 2f)));
        //Go for closest altar
        agent.SetDestination(GM.Instance.GetClosestAltar(this.transform.position).transform.position);
    }
    public void Throw(Vector3 direction) {
        chargeDirection = direction;
        SetState(UnitState.charging);
    }
    public void Throw(Altar target) {
        SetTargetAltar(target);
    }

    public Altar LookForTargets() {
        Altar closestAltar = GM.Instance.GetClosestAltar(this.transform.position);
        if (Vector3.Distance(closestAltar.transform.position,this.transform.position) < chargingDetectionRange) {
            SetTargetAltar(closestAltar);
            return closestAltar;
        } else {
            return null;
        }
    }
    public void SetTargetAltar(Altar altar) {
        targetAltar = altar;
        if (agent != null) {//HACK - Why do we have to check this? Destroyed not applied?
            agent.SetDestination(altar.transform.position);
        }
        SetState(UnitState.garrisoning);
    }

    public void UpdateSkin() {
        for (int i = 0; i < 2; i++) {
            skins[i].SetActive(i == team);
        }
    }
    public void LevelUp() {
        level++;
        //TODO - Update size
        this.transform.localScale += Vector3.one * 0.5f;
    }
    public void SetHighlight(bool isHighlighted) {
        if (isHighlighted) {
            foreach (GameObject s in skins) {
                if (s.activeSelf) {
                    s.GetComponent<Renderer>().material.SetFloat("_Highlight", 1f);
                }
            }
        } else {
            foreach (GameObject s in skins) {
                if (s.activeSelf) {
                    s.GetComponent<Renderer>().material.SetFloat("_Highlight", 0f);
                }
            }
        }
    }

    public static Slime SpawnSlime(Vector3 position, int team) {
        GameObject newSlime = Instantiate(GM.Instance.slimePrefab, position, Quaternion.identity);
        newSlime.GetComponent<Slime>().team = team;
        return newSlime.GetComponent<Slime>();
    }

    public void GarrisonSlime() {
        //TODO - add some VFX and SFX
        DestroySlime();
    }
    public void SacrificeSlime() {
        //TODO - add some VFX and SFX
        DestroySlime();
    }
    public void CheckForMerges() {
        //print("Trying to merge!");
        Slime closestAlly =  GM.Instance.GetClosestAlliedSlime(this);
        if (closestAlly!=null) {
            if (Vector3.Distance(this.transform.position, closestAlly.transform.position) < (mergeDistance + 0.05f*level)) {
                MergeSlime(closestAlly);
            }
        }
    }
    public void MergeSlime(Slime other) {
        //print("MERGE");
        other.DestroySlime();
        LevelUp();
    }

    public void DestroySlime() {
        GM.Instance.RemoveUnit(this);
        Destroy(this.gameObject);
    }

}
